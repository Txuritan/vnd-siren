extern crate vnd_siren;

#[macro_use]
extern crate serde_json;

use serde_json::to_value;

use vnd_siren::prelude::*;

#[test]
fn example() {
    let siren: Entity = Entity::builder()
        .class("order")
        .properties(vec![
            Property::new("itemCount", 3),
            Property::new("orderNumber", 42),
            Property::new("status", "pending"),
        ])
        .actions(vec![
            Action::builder("add-item", "http://api.x.io/orders/42/items")
                .method(Method::Post)
                .title("Add Item")
                .typ("application/x-www-form-urlencoded")
                .fields(vec![
                    Field::builder("orderNumber").typ("hidden").value("42"),
                    Field::builder("productCode").typ("text"),
                    Field::builder("quantity").typ("number"),
                ]),
        ])
        .entities(vec![
            Entity::builder()
                .rel(vec!["http://x.io/rels/order-items"])
                .classes(vec!["items", "collection"])
                .href("http://api.x.io/orders/42/items"),
            Entity::builder()
                .rel(vec!["http://x.io/rels/customer"])
                .classes(vec!["info", "customer"])
                .links(vec![
                    Link::builder(vec!["self"], "http://api.x.io/customers/pj123"),
                ])
                .properties(vec![
                    Property::new("customerId", "pj123"),
                    Property::new("name", "Peter Joseph"),
                ]),
        ])
        .links(vec![
            Link::builder(vec!["self"], "http://api.x.io/orders/42"),
            Link::builder(vec!["previous"], "http://api.x.io/orders/41"),
            Link::builder(vec!["next"], "http://api.x.io/orders/43"),
        ])
        .into();

    assert_eq!(
        json!({
            "actions": [{
                "fields": [
                    { "name": "orderNumber", "type": "hidden", "value": "42" },
                    { "name": "productCode", "type": "text" },
                    { "name": "quantity", "type": "number" }
                ],
                "href": "http://api.x.io/orders/42/items",
                "method": "POST",
                "name": "add-item",
                "title": "Add Item",
                "type": "application/x-www-form-urlencoded"
            }],
            "class": [ "order" ],
            "entities": [{
                "class": [ "items", "collection" ],
                "href": "http://api.x.io/orders/42/items",
                "rel": [ "http://x.io/rels/order-items" ]
            }, {
                "class": [ "info", "customer" ],
                "links": [
                    { "href": "http://api.x.io/customers/pj123", "rel": [ "self" ] }
                ],
                "properties": {
                    "customerId": "pj123",
                    "name": "Peter Joseph"
                },
                "rel": [ "http://x.io/rels/customer" ]
            }],
            "links": [
                { "href": "http://api.x.io/orders/42", "rel": [ "self" ] },
                { "href": "http://api.x.io/orders/41", "rel": [ "previous" ] },
                { "href": "http://api.x.io/orders/43", "rel": [ "next" ] }
            ],
            "properties": {
                "itemCount": 3,
                "orderNumber": 42,
                "status": "pending"
            }
        }),
        to_value(&siren).unwrap(),
    );
}