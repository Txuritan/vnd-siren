use std::collections::HashMap;

use serde_json::value::Value;

use action::Action;
use link::Link;
use property::Property;

/// An Entity is a URI-addressable resource that has properties and actions associated with it.
/// It may contain sub-entities and navigational links.
/// 
/// Root entities and sub-entities that are embedded representations SHOULD contain a `links` collection with at least one item contain a `rel` value of `self` and an `href` attribute with a value of the entity's URI.
/// 
/// Sub-entities that are embedded links MUST contain an `href` attribute with a value of its URI.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Entity {
    /// A collection of action objects, represented in JSON Siren as an array such as `{ "actions": [{ ... }] }`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub actions: Option<Vec<Action>>,

    /// Describes the nature of an entity's content based on the current representation.
    /// Possible values are implementation-dependent and should be documented.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub class: Option<Vec<String>>,

    /// A collection of related sub-entities. If a sub-entity contains an `href` value, it should be treated as an embedded link.
    /// Clients may choose to optimistically load embedded links.
    /// If no `href` value exists, the sub-entity is an embedded entity representation that contains all the characteristics of a typical entity.
    /// One difference is that a sub-entity MUST contain a `rel` attribute to describe its relationship to the parent entity.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entities: Option<Vec<Box<Entity>>>,

    /// The URI of the linked sub-entity.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub href: Option<String>,

    /// A collection of items that describe navigational links, distinct from entity relationships.
    /// Link items should contain a `rel` attribute to describe the relationship and an href attribute to point to the target URI.
    /// Entities should include a link `rel` to `self`.
    /// In JSON Siren, this is represented as `"links": [{ "rel": ["self"], "href": "http://api.x.io/orders/1234" }]`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub links: Option<Vec<Link>>,

    /// A set of key-value pairs that describe the state of an entity.
    /// In JSON Siren, this is an object such as `{ "name": "Kevin", "age": 30 }`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<HashMap<String, Value>>,

    /// Defines the relationship of the sub-entity to its parent, per [Web Linking (RFC5988)](http://tools.ietf.org/html/rfc5988) and [Link Relations](http://www.iana.org/assignments/link-relations/link-relations.xhtml).
    #[serde(skip_serializing_if = "Option::is_none")]
    pub rel: Option<Vec<String>>,

    /// Descriptive text about the entity.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
}

impl Entity {
    pub fn builder() -> EntityBuilder {
        EntityBuilder {
            actions: None,
            class: None,
            entities: None,
            href: None,
            links: None,
            properties: None,
            rel: None,
            title: None,
        }
    }

    /// Returns a reference to the Entity's Actions.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .actions(vec![
    ///         Action::builder("add-item", "http://api.x.io/orders/42/items"),
    ///     ]).into();
    /// 
    /// assert_eq!(
    ///     &Some(vec![
    ///         Action::builder("add-item", "http://api.x.io/orders/42/items").into(),
    ///     ]),
    ///     entity.actions(),
    /// );
    /// ```
    pub fn actions(&self) -> &Option<Vec<Action>> {
        &self.actions
    }

    /// Returns a reference to the Entity's classes.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .classes(vec!["item"]).into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), entity.classes());
    /// ```
    pub fn classes(&self) -> &Option<Vec<String>> {
        &self.class
    }

    /// Returns a reference to the Entity's Entities.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .entities(vec![Entity::builder()]).into();
    /// 
    /// assert_eq!(&Some(vec![Box::new(Entity::builder().into())]), entity.entities());
    /// ```
    pub fn entities(&self) -> &Option<Vec<Box<Entity>>> {
        &self.entities
    }

    /// Returns a reference to the Entity's href.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .href("http://api.x.io/orders/42/items").into();
    /// 
    /// assert_eq!(&Some("http://api.x.io/orders/42/items".to_string()), entity.href());
    /// ```
    pub fn href(&self) -> &Option<String> {
        &self.href
    }

    /// Returns a reference to the Entity's Links.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .links(vec![
    ///         Link::builder(vec!["self"], "http://api.x.io/customers/pj123")
    ///     ]).into();
    /// ```
    pub fn links(&self) -> &Option<Vec<Link>> {
        &self.links
    }

    /// Returns a reference to the Entity's Properties.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .properties(vec![
    ///         Property::new("customerId", "pj123"),
    ///         Property::new("name", "Peter Joseph"),
    ///     ]).into();
    /// ```
    pub fn properties(&self) -> &Option<HashMap<String, Value>> {
        &self.properties
    }

    /// Returns a reference to the Entity's rel vector.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .rel(vec!["http://x.io/rels/customer"]).into();
    /// 
    /// assert_eq!(&Some(vec!["http://x.io/rels/customer".to_string()]), entity.rel());
    /// ```
    pub fn rel(&self) -> &Option<Vec<String>> {
        &self.rel
    }

    /// Returns a reference to the Entity's title.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .title("example").into();
    /// 
    /// assert_eq!(&Some("example".to_string()), entity.title());
    /// ```
    pub fn title(&self) -> &Option<String> {
        &self.title
    }
}

/// A builder of Entities.
#[derive(Clone, Debug, PartialEq)]
pub struct EntityBuilder {
    /// A collection of action objects, represented in JSON Siren as an array such as `{ "actions": [{ ... }] }`.
    actions: Option<Vec<Action>>,

    /// Describes the nature of an entity's content based on the current representation.
    /// Possible values are implementation-dependent and should be documented.
    class: Option<Vec<String>>,

    /// A collection of related sub-entities. If a sub-entity contains an `href` value, it should be treated as an embedded link.
    /// Clients may choose to optimistically load embedded links.
    /// If no `href` value exists, the sub-entity is an embedded entity representation that contains all the characteristics of a typical entity.
    /// One difference is that a sub-entity MUST contain a `rel` attribute to describe its relationship to the parent entity.
    entities: Option<Vec<Box<Entity>>>,

    /// The URI of the linked sub-entity.
    href: Option<String>,

    /// A collection of items that describe navigational links, distinct from entity relationships.
    /// Link items should contain a `rel` attribute to describe the relationship and an href attribute to point to the target URI.
    /// Entities should include a link `rel` to `self`.
    /// In JSON Siren, this is represented as `"links": [{ "rel": ["self"], "href": "http://api.x.io/orders/1234" }]`.
    links: Option<Vec<Link>>,

    /// A set of key-value pairs that describe the state of an entity.
    /// In JSON Siren, this is an object such as `{ "name": "Kevin", "age": 30 }`.
    properties: Option<HashMap<String, Value>>,

    /// Defines the relationship of the sub-entity to its parent, per [Web Linking (RFC5988)](http://tools.ietf.org/html/rfc5988) and [Link Relations](http://www.iana.org/assignments/link-relations/link-relations.xhtml).
    rel: Option<Vec<String>>,

    /// Descriptive text about the entity.
    title: Option<String>,
}

impl EntityBuilder {
    /// Adds and Action to the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .action(
    ///         Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     ).into();
    /// 
    /// assert_eq!(
    ///     &Some(vec![
    ///         Action::builder("add-item", "http://api.x.io/orders/42/items").into(),
    ///     ]),
    ///     entity.actions(),
    /// );
    /// ```
    pub fn action(self, action: impl Into<Action>) -> Self {
        self.actions(vec![action])
    }

    /// Adds a list of Actions to the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .actions(vec![
    ///         Action::builder("add-item", "http://api.x.io/orders/42/items"),
    ///     ]).into();
    /// 
    /// assert_eq!(
    ///     &Some(vec![
    ///         Action::builder("add-item", "http://api.x.io/orders/42/items").into(),
    ///     ]),
    ///     entity.actions(),
    /// );
    /// ```
    pub fn actions(mut self, actions: Vec<impl Into<Action>>) -> Self {
        if let Some(ref mut s_action) = self.actions {
            for action in actions.into_iter() {
                s_action.push(action.into());
            }
        } else {
            self.actions = Some(actions.into_iter().map(|a| a.into()).collect());
        }

        self
    }

    /// Add a class to the Entity
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .class("item").into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), entity.classes());
    /// ```
    pub fn class(self, class: impl Into<String>) -> Self {
        self.classes(vec![class])
    }

    /// Add a vector of classes to the Entity
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .classes(vec!["item"]).into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), entity.classes());
    /// ```
    pub fn classes(mut self, classes: Vec<impl Into<String>>) -> Self {
        if let Some(ref mut s_class) = self.class {
            for class in classes.into_iter() {
                s_class.push(class.into());
            }
        } else {
            self.class = Some(classes.into_iter().map(|c| c.into()).collect());
        }

        self
    }

    /// Add an Entity to the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .entity(Entity::builder()).into();
    /// 
    /// assert_eq!(&Some(vec![Box::new(Entity::builder().into())]), entity.entities());
    /// ```
    pub fn entity(self, entity: impl Into<Entity>) -> Self {
        self.entities(vec![entity])
    }

    /// Add a vector of Entities to the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .entities(vec![Entity::builder()]).into();
    /// 
    /// assert_eq!(&Some(vec![Box::new(Entity::builder().into())]), entity.entities());
    /// ```
    pub fn entities(mut self, entities: Vec<impl Into<Entity>>) -> Self {
        if let Some(ref mut s_entities) = self.entities {
            for entity in entities.into_iter() {
                s_entities.push(Box::new(entity.into()));
            }
        } else {
            self.entities = Some(entities.into_iter().map(|e| Box::new(e.into())).collect());
        }

        self
    }

    /// Set the Entity's href.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .href("http://api.x.io/orders/42/items").into();
    /// 
    /// assert_eq!(&Some("http://api.x.io/orders/42/items".to_string()), entity.href());
    /// ```
    pub fn href(mut self, href: impl Into<String>) -> Self {
        self.href = Some(href.into());

        self
    }

    /// Add a Link to the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .link(Link::builder(vec!["self"], "http://api.x.io/customers/pj123"))
    ///     .into();
    /// 
    /// assert_eq!(
    ///     &Some(vec![
    ///         Link::builder(vec!["self"], "http://api.x.io/customers/pj123").into()
    ///     ]),
    ///     entity.links(),
    /// );
    /// ```
    pub fn link(self, link: impl Into<Link>) -> Self {
        self.links(vec![link])
    }

    /// Add a vector of Links to the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .links(vec![
    ///         Link::builder(vec!["self"], "http://api.x.io/customers/pj123")
    ///     ]).into();
    /// 
    /// assert_eq!(
    ///     &Some(vec![
    ///         Link::builder(vec!["self"], "http://api.x.io/customers/pj123").into()
    ///     ]),
    ///     entity.links(),
    /// );
    /// ```
    pub fn links(mut self, links: Vec<impl Into<Link>>) -> Self {
        if let Some(ref mut s_links) = self.links {
            for link in links.into_iter() {
                s_links.push(link.into());
            }
        } else {
            self.links = Some(links.into_iter().map(|l| l.into()).collect());
        }

        self
    }

    /// Add a Property to the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .property("customerId", "pj123")
    ///     .property("name", "Peter Joseph")
    ///     .into();
    /// ```
    pub fn property(mut self, key: impl Into<String>, value: impl Into<Value>) -> Self {
        if let Some(ref mut s_properties) = self.properties {
            s_properties.insert(key.into(), value.into());
        } else {
            let mut s_properties = HashMap::new();

            s_properties.insert(key.into(), value.into());

            self.properties = Some(s_properties);
        }

        self
    }

    /// Add a vector of Properties to the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .properties(vec![
    ///         Property::new("customerId", "pj123"),
    ///         Property::new("name", "Peter Joseph"),
    ///     ]).into();
    /// ```
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .properties(vec![
    ///         ("customerId", "pj123"),
    ///         ("name", "Peter Joseph"),
    ///     ]).into();
    /// ```
    pub fn properties(mut self, properties: Vec<impl Into<Property>>) -> Self {
        if let Some(ref mut s_properties) = self.properties {
            for property in properties.into_iter() {
                let (key, value) = property.into().get();
                s_properties.insert(key.into(), value.into());
            }
        } else {
            let mut s_properties = HashMap::new();

            for property in properties.into_iter() {
                let (key, value) = property.into().get();
                s_properties.insert(key.into(), value.into());
            }

            self.properties = Some(s_properties);
        }

        self
    }

    /// Add a vector of Rels for the Entity.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .rel(vec!["http://x.io/rels/customer"]).into();
    /// 
    /// assert_eq!(&Some(vec!["http://x.io/rels/customer".to_string()]), entity.rel());
    /// ```
    pub fn rel(mut self, rels: Vec<impl Into<String>>) -> Self {
        if let Some(ref mut s_rel) = self.rel {
            for rel in rels.into_iter() {
                s_rel.push(rel.into());
            }
        } else {
            self.rel = Some(rels.into_iter().map(|r| r.into()).collect());
        }

        self
    }

    /// Set the Entity's title.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let entity: Entity = Entity::builder()
    ///     .title("example").into();
    /// 
    /// assert_eq!(&Some("example".to_string()), entity.title());
    /// ```
    pub fn title(mut self, title: impl Into<String>) -> Self {
        self.title = Some(title.into());

        self
    }
}

impl From<EntityBuilder> for Entity {
    fn from(builder: EntityBuilder) -> Entity {
        Entity {
            actions: builder.actions,
            class: builder.class,
            entities: builder.entities,
            href: builder.href,
            links: builder.links,
            properties: builder.properties,
            rel: builder.rel,
            title: builder.title,
        }
    }
}