#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Link {
    #[serde(skip_serializing_if = "Option::is_none")]
    class: Option<Vec<String>>,
    href: String,
    rel: Vec<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "type")]
    typ: Option<String>,
}

impl Link {
    pub fn builder(rel: Vec<impl Into<String>>, href: impl Into<String>) -> LinkBuilder {
        LinkBuilder {
            class: None,
            href: href.into(),
            rel: rel.into_iter().map(|s| s.into()).collect(),
            title: None,
            typ: None,
        }
    }

    /// Returns a reference to the Link's Classes.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let link: Link = Link::builder(vec!["self"], "http://api.x.io/orders/42")
    ///     .classes(vec!["item"]).into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), link.classes());
    /// ```
    pub fn classes(&self) -> &Option<Vec<String>> {
        &self.class
    }

    /// Returns a reference to the Link's title.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let link: Link = Link::builder(vec!["self"], "http://api.x.io/orders/42")
    ///     .title("Link").into();
    /// 
    /// assert_eq!(&Some("Link".to_string()), link.title());
    /// ```
    pub fn title(&self) -> &Option<String> {
        &self.title
    }

    /// Returns a reference to the Link's type.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let link: Link = Link::builder(vec!["self"], "http://api.x.io/orders/42")
    ///     .typ("application/x-www-form-urlencoded")
    ///     .into();
    /// 
    /// assert_eq!(&Some("application/x-www-form-urlencoded".to_string()), link.typ());
    /// ```
    pub fn typ(&self) -> &Option<String> {
        &self.typ
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct LinkBuilder {
    class: Option<Vec<String>>,
    href: String,
    rel: Vec<String>,
    title: Option<String>,
    typ: Option<String>,
}

impl LinkBuilder {
    /// Add a Class to the Link.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let link: Link = Link::builder(vec!["self"], "http://api.x.io/orders/42")
    ///     .class("item").into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), link.classes());
    /// ```
    pub fn class(mut self, class: impl Into<String>) -> Self {
        if let Some(ref mut s_class) = self.class {
            s_class.push(class.into());
        } else {
            self.class = Some(vec![class.into()])
        }

        self
    }

    /// Add a vector of Classes to the Link.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let link: Link = Link::builder(vec!["self"], "http://api.x.io/orders/42")
    ///     .classes(vec!["item"]).into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), link.classes());
    /// ```
    pub fn classes(mut self, classes: Vec<impl Into<String>>) -> Self {
        if let Some(ref mut s_class) = self.class {
            for class in classes.into_iter() {
                s_class.push(class.into());
            }
        } else {
            self.class = Some(classes.into_iter().map(|c| c.into()).collect());
        }

        self
    }

    /// Set the title of the Link.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let link: Link = Link::builder(vec!["self"], "http://api.x.io/orders/42")
    ///     .title("Link").into();
    /// 
    /// assert_eq!(&Some("Link".to_string()), link.title());
    /// ```
    pub fn title(mut self, title: impl Into<String>) -> Self {
        self.title = Some(title.into());

        self
    }

    /// Set the Link's type.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let link: Link = Link::builder(vec!["self"], "http://api.x.io/orders/42")
    ///     .typ("application/x-www-form-urlencoded")
    ///     .into();
    /// 
    /// assert_eq!(&Some("application/x-www-form-urlencoded".to_string()), link.typ());
    /// ```
    pub fn typ(mut self, typ: impl Into<String>) -> Self {
        self.typ = Some(typ.into());

        self
    }
}

impl From<LinkBuilder> for Link {
    fn from(builder: LinkBuilder) -> Link {
        Link {
            class: builder.class,
            href: builder.href,
            rel: builder.rel,
            title: builder.title,
            typ: builder.typ,
        }
    }
}
