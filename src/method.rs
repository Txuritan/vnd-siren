use std::fmt;

#[cfg(feature="http-compat")]
use http::Method as HttpMethod;

/// The Request Method (VERB)
/// 
/// This type also contains constants for a number of common HTTP methods such as GET, POST, etc.
#[derive(Clone, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
#[serde(rename_all = "SCREAMING_SNAKE_CASE")]
pub enum Method {
    Get,
    Post,
    Put,
    Delete,
    Patch,
    Other(String),
}

impl Method {
    /// Convert Siren method into HTTP/Hyper method
    /// 
    /// # Panics
    /// 
    /// This will panic if the method uses the `Method::Other` variant.
    pub fn as_http(&self) -> HttpMethod {
        match self {
            Method::Get => HttpMethod::GET,
            Method::Post => HttpMethod::POST,
            Method::Put => HttpMethod::PUT,
            Method::Delete => HttpMethod::DELETE,
            Method::Patch => HttpMethod::PATCH,
            _ => panic!("Custom HTTP method not allowed"),
        }
    }

    /// Returns true if the method is `GET`
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let get = Method::Get;
    /// 
    /// assert!(get.is_get());
    /// ```
    pub fn is_get(&self) -> bool {
        self == &Method::Get
    }

    /// Returns true if the method is `POST`
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let post = Method::Post;
    /// 
    /// assert!(post.is_post());
    /// ```
    pub fn is_post(&self) -> bool {
        self == &Method::Post
    }

    /// Returns true if the method is `PUT`
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let put = Method::Put;
    /// 
    /// assert!(put.is_put());
    /// ```
    pub fn is_put(&self) -> bool {
        self == &Method::Put
    }

    /// Returns true if the method is `DELETE`
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let delete = Method::Delete;
    /// 
    /// assert!(delete.is_delete());
    /// ```
    pub fn is_delete(&self) -> bool {
        self == &Method::Delete
    }

    /// Returns true if the method is `PATCH`
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let patch = Method::Patch;
    /// 
    /// assert!(patch.is_patch());
    /// ```
    pub fn is_patch(&self) -> bool {
        self == &Method::Patch
    }

    /// Returns true if the `Method::Other` variant matches the given string
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let other = Method::Other("other".to_string());
    /// 
    /// assert!(other.is_other("other"));
    /// ```
    pub fn is_other(&self, method: impl Into<String>) -> bool {
        match self {
            Method::Other(ref other) => {
                other == &method.into()
            },
            _ => false,
        }
    }
}

impl fmt::Display for Method {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Method::Get => write!(f, "GET"),
            Method::Post => write!(f, "POST"),
            Method::Put => write!(f, "PUT"),
            Method::Delete => write!(f, "DELETE"),
            Method::Patch => write!(f, "PATCH"),
            Method::Other(ref other) => write!(f, "{}", other),
        }
    }
}

impl Into<HttpMethod> for Method {
    fn into(self) -> HttpMethod {
        self.as_http()
    }
}