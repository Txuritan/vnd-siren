use serde_json::value::Value;

#[derive(Debug)]
pub struct Property {
    key: String,
    value: Value,
}

impl Property {
    /// Create a new property with the given key and value.
    /// 
    /// Value ***must*** be compatible with `serde_json`'s `Value`.
    pub fn new(key: impl Into<String>, value: impl Into<Value>) -> Self {
        Self {
            key: key.into(),
            value: value.into(),
        }
    }

    pub(crate) fn get(self) -> (String, Value) {
        (self.key, self.value)
    }
}

impl<
    K: Into<String>,
    V: Into<Value>,
> From<(K, V)> for Property {
    fn from((key, value): (K, V)) -> Property {
        Property {
            key: key.into(),
            value: value.into(),
        }
    }
}