use field::Field;
use method::Method;

/// Actions show available behaviors an entity exposes.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Action {
    /// Describes the nature of an action based on the current representation.
    /// Possible values are implementation-dependent and should be documented.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub class: Option<Vec<String>>,

    /// A collection of fields, expressed as an array of objects in JSON Siren such as `{ "fields" : [{ ... }] }`.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<Vec<Field>>,

    /// A string that identifies the action to be performed.
    /// Action names MUST be unique within the set of actions for an entity.
    /// The behavior of clients when parsing a Siren document that violates this constraint is undefined. 
    pub name: String,

    /// The URI of the action.
    pub href: String,

    /// An enumerated attribute mapping to a protocol method.
    /// For HTTP, these values may be `GET`, `PUT`, `POST`, `DELETE`, or `PATCH`.
    /// As new methods are introduced, this list can be extended.
    /// If this attribute is omitted, `GET` should be assumed.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub method: Option<Method>,

    /// Descriptive text about the action.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,

    /// The encoding type for the request.
    /// When omitted and the fields attribute exists, the default value is `application/x-www-form-urlencoded`.
    #[serde(skip_serializing_if = "Option::is_none", rename = "type")]
    pub typ: Option<String>,
}

impl Action {
    /// Create a Action builder with the given name and href.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action = Action::builder("add-item", "http://api.x.io/orders/42/items");
    /// ```
    pub fn builder(name: impl Into<String>, href: impl Into<String>) -> ActionBuilder {
        ActionBuilder {
            class: None,
            fields: None,
            name: name.into(),
            href: href.into(),
            method: None,
            title: None,
            typ: None,
        }
    }

    /// Returns a reference to the Action's Classes.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .classes(vec!["item"]).into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), action.classes());
    /// ```
    pub fn classes(&self) -> &Option<Vec<String>> {
        &self.class
    }

    /// Returns a reference to the Action's Fields.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .fields(vec![
    ///         Field::builder("orderNumber").typ("hidden").value("42"),
    ///         Field::builder("productCode").typ("text"),
    ///         Field::builder("quantity").typ("number"),
    ///     ]).into();
    /// 
    /// assert_eq!(
    ///     &Some(vec![
    ///         Field::builder("orderNumber").typ("hidden").value("42").into(),
    ///         Field::builder("productCode").typ("text").into(),
    ///         Field::builder("quantity").typ("number").into(),
    ///     ]),
    ///     action.fields(),
    /// );
    /// ```
    pub fn fields(&self) -> &Option<Vec<Field>> {
        &self.fields
    }

    /// Returns a reference to the Action's Method.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .method(Method::Post).into();
    /// 
    /// assert_eq!(&Some(Method::Post), action.method());
    /// ```
    pub fn method(&self) -> &Option<Method> {
        &self.method
    }

    /// Returns a reference to the Action's title.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .title("Add Item").into();
    /// 
    /// assert_eq!(&Some("Add Item".to_string()), action.title());
    /// ```
    pub fn title(&self) -> &Option<String> {
        &self.title
    }

    /// Returns a reference to the Action's type.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .typ("application/x-www-form-urlencoded").into();
    /// 
    /// assert_eq!(&Some("application/x-www-form-urlencoded".to_string()), action.typ());
    /// ```
    // Change this to an enum?
    pub fn typ(&self) -> &Option<String> {
        &self.typ
    }
}

/// A builder of Actions
#[derive(Clone, Debug, PartialEq)]
pub struct ActionBuilder {
    /// Describes the nature of an action based on the current representation.
    /// Possible values are implementation-dependent and should be documented.
    pub class: Option<Vec<String>>,

    /// A collection of fields, expressed as an array of objects in JSON Siren such as `{ "fields" : [{ ... }] }`.
    pub fields: Option<Vec<Field>>,

    /// A string that identifies the action to be performed.
    /// Action names MUST be unique within the set of actions for an entity.
    /// The behavior of clients when parsing a Siren document that violates this constraint is undefined. 
    pub name: String,

    /// The URI of the action.
    pub href: String,

    /// An enumerated attribute mapping to a protocol method.
    /// For HTTP, these values may be `GET`, `PUT`, `POST`, `DELETE`, or `PATCH`.
    /// As new methods are introduced, this list can be extended.
    /// If this attribute is omitted, `GET` should be assumed.
    pub method: Option<Method>,

    /// Descriptive text about the action.
    pub title: Option<String>,

    /// The encoding type for the request.
    /// When omitted and the fields attribute exists, the default value is `application/x-www-form-urlencoded`.
    pub typ: Option<String>,
}

impl ActionBuilder {
    /// Add a Class to the Action.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .class("item").into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), action.classes());
    /// ```
    pub fn class(mut self, class: impl Into<String>) -> Self {
        if let Some(ref mut s_class) = self.class {
            s_class.push(class.into());
        } else {
            self.class = Some(vec![class.into()])
        }

        self
    }

    /// Add a vector of Classes to the Action.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .classes(vec!["item"]).into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), action.classes());
    /// ```
    pub fn classes(mut self, classes: Vec<impl Into<String>>) -> Self {
        if let Some(ref mut s_class) = self.class {
            for class in classes.into_iter() {
                s_class.push(class.into());
            }
        } else {
            self.class = Some(classes.into_iter().map(|f| f.into()).collect());
        }

        self
    }

    /// Add a Field to the Action.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .field(Field::builder("orderNumber").typ("hidden").value("42"))
    ///     .field(Field::builder("productCode").typ("text"))
    ///     .field(Field::builder("quantity").typ("number"))
    ///     .into();
    /// 
    /// assert_eq!(
    ///     &Some(vec![
    ///         Field::builder("orderNumber").typ("hidden").value("42").into(),
    ///         Field::builder("productCode").typ("text").into(),
    ///         Field::builder("quantity").typ("number").into(),
    ///     ]),
    ///     action.fields(),
    /// );
    /// ```
    pub fn field(mut self, field: impl Into<Field>) -> Self {
        if let Some(ref mut s_fields) = self.fields {
            s_fields.push(field.into());
        } else {
            self.fields = Some(vec![field.into()])
        }

        self
    }

    /// Add a vector of Fields to the Action.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .fields(vec![
    ///         Field::builder("orderNumber").typ("hidden").value("42"),
    ///         Field::builder("productCode").typ("text"),
    ///         Field::builder("quantity").typ("number"),
    ///     ]).into();
    /// 
    /// assert_eq!(
    ///     &Some(vec![
    ///         Field::builder("orderNumber").typ("hidden").value("42").into(),
    ///         Field::builder("productCode").typ("text").into(),
    ///         Field::builder("quantity").typ("number").into(),
    ///     ]),
    ///     action.fields(),
    /// );
    /// ```
    pub fn fields(mut self, fields: Vec<impl Into<Field>>) -> Self {
        if let Some(ref mut s_fields) = self.fields {
            for field in fields.into_iter() {
                s_fields.push(field.into());
            }
        } else {
            self.fields = Some(fields.into_iter().map(|f| f.into()).collect());
        }

        self
    }

    /// Set the Method of the Action.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .method(Method::Post).into();
    /// 
    /// assert_eq!(&Some(Method::Post), action.method());
    /// ```
    pub fn method(mut self, method: Method) -> Self {
        self.method = Some(method);

        self
    }

    /// Set the title of the Action.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .title("Add Item").into();
    /// 
    /// assert_eq!(&Some("Add Item".to_string()), action.title());
    /// ```
    pub fn title(mut self, title: impl Into<String>) -> Self {
        self.title = Some(title.into());

        self
    }

    /// Set the Action's type.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let action: Action = Action::builder("add-item", "http://api.x.io/orders/42/items")
    ///     .typ("application/x-www-form-urlencoded")
    ///     .into();
    /// 
    /// assert_eq!(&Some("application/x-www-form-urlencoded".to_string()), action.typ());
    /// ```
    pub fn typ(mut self, typ: impl Into<String>) -> Self {
        self.typ = Some(typ.into());

        self
    }
}

impl From<ActionBuilder> for Action {
    fn from(builder: ActionBuilder) -> Action {
        Action {
            class: builder.class,
            fields: builder.fields,
            name: builder.name,
            href: builder.href,
            method: builder.method,
            title: builder.title,
            typ: builder.typ,
        }
    }
}
