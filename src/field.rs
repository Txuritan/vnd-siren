#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Field {
    #[serde(skip_serializing_if = "Option::is_none")]
    class: Option<Vec<String>>,
    name: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none", rename = "type")]
    typ: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    value: Option<String>,
}

impl Field {
    pub fn builder(name: impl Into<String>) -> FieldBuilder {
        FieldBuilder {
            class: None,
            name: name.into(),
            title: None,
            typ: None,
            value: None,
        }
    }

    /// Returns a reference to the Fields's Classes.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .classes(vec!["item"]).into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), field.classes());
    /// ```
    pub fn classes(&self) -> &Option<Vec<String>> {
        &self.class
    }

    /// Returns a reference to the Fields's title.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .title("field").into();
    /// 
    /// assert_eq!(&Some("field".to_string()), field.title());
    /// ```
    pub fn title(&self) -> &Option<String> {
        &self.title
    }

    /// Returns a reference to the Fields's type.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .typ("number").into();
    /// 
    /// assert_eq!(&Some("number".to_string()), field.typ());
    /// ```
    pub fn typ(&self) -> &Option<String> {
        &self.typ
    }

    /// Returns a reference to the Fields's value.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .value("42").into();
    /// 
    /// assert_eq!(&Some("42".to_string()), field.value());
    /// ```
    pub fn value(&self) -> &Option<String> {
        &self.value
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct FieldBuilder {
    class: Option<Vec<String>>,
    name: String,
    title: Option<String>,
    typ: Option<String>,
    value: Option<String>,
}

impl FieldBuilder {
    /// Add a Class to the Field.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .class("item").into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), field.classes());
    /// ```
    pub fn class(mut self, class: impl Into<String>) -> Self {
        if let Some(ref mut s_class) = self.class {
            s_class.push(class.into());
        } else {
            self.class = Some(vec![class.into()])
        }

        self
    }

    /// Add a vector of Classes to the Field.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .classes(vec!["item"]).into();
    /// 
    /// assert_eq!(&Some(vec!["item".to_string()]), field.classes());
    /// ```
    pub fn classes(mut self, classes: Vec<impl Into<String>>) -> Self {
        if let Some(ref mut s_class) = self.class {
            for class in classes.into_iter() {
                s_class.push(class.into());
            }
        } else {
            self.class = Some(classes.into_iter().map(|c| c.into()).collect());
        }

        self
    }

    /// Set the title of the Field.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .title("field").into();
    /// 
    /// assert_eq!(&Some("field".to_string()), field.title());
    /// ```
    pub fn title(mut self, title: impl Into<String>) -> Self {
        self.title = Some(title.into());

        self
    }

    /// Set the Field's type.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .typ("number")
    ///     .into();
    /// 
    /// assert_eq!(&Some("number".to_string()), field.typ());
    /// ```
    pub fn typ(mut self, typ: impl Into<String>) -> Self {
        self.typ = Some(typ.into());

        self
    }

    /// Sets the Field's value.
    /// 
    /// # Examples
    /// 
    /// ```rust
    /// # extern crate vnd_siren;
    /// # use vnd_siren::prelude::*;
    /// let field: Field = Field::builder("orderNumber")
    ///     .value("42").into();
    /// 
    /// assert_eq!(&Some("42".to_string()), field.value());
    /// ```
    pub fn value(mut self, value: impl Into<String>) -> Self {
        self.value = Some(value.into());

        self
    }
}

impl From<FieldBuilder> for Field {
    fn from(builder: FieldBuilder) -> Field {
        Field {
            class: builder.class,
            name: builder.name,
            title: builder.title,
            typ: builder.typ,
            value: builder.value,
        }
    }
}