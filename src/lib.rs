//! Siren is a hypermedia specification for representing entities.
//! As HTML is used for visually representing documents on a Web site, Siren is a specification for presenting entities via a Web API.
//! Siren offers structures to communicate information about entities, actions for executing state transitions, and links for client navigation.
//! 
//! Siren is intended to be a general specification of a generic media type that can be applied to other types that are not inherently hypermedia-powered.
//! The initial implementation is JSON Siren.
//! Other implementations, such as XML Siren, may also be implemented using the Siren specification.
//! 
//! # Examples
//! 
//! Recreating the example from the Siren GitHub.
//! 
//! ```rust
//! # extern crate vnd_siren;
//! # use vnd_siren::prelude::*;
//! let siren = Entity::builder()
//!     .class("order")
//!     .properties(vec![
//!         Property::new("itemCount", 3),
//!         Property::new("orderNumber", 42),
//!         Property::new("status", "pending"),
//!     ])
//!     .actions(vec![
//!         Action::builder("add-item", "http://api.x.io/orders/42/items")
//!             .method(Method::Post)
//!             .title("Add Item")
//!             .typ("application/x-www-form-urlencoded")
//!             .fields(vec![
//!                 Field::builder("orderNumber").typ("hidden").value("42"),
//!                 Field::builder("productCode").typ("text"),
//!                 Field::builder("quantity").typ("number"),
//!             ]),
//!     ])
//!     .entities(vec![
//!         Entity::builder()
//!             .rel(vec!["http://x.io/rels/order-items"])
//!             .classes(vec!["items", "collection"])
//!             .href("http://api.x.io/orders/42/items"),
//!         Entity::builder()
//!             .rel(vec!["http://x.io/rels/customer"])
//!             .classes(vec!["info", "customer"])
//!             .links(vec![
//!                 Link::builder(vec!["self"], "http://api.x.io/customers/pj123"),
//!             ])
//!             .properties(vec![
//!                 Property::new("customerId", "pj123"),
//!                 Property::new("name", "Peter Joseph"),
//!             ]),
//!     ])
//!     .links(vec![
//!         Link::builder(vec!["self"], "http://api.x.io/orders/42"),
//!         Link::builder(vec!["previous"], "http://api.x.io/orders/41"),
//!         Link::builder(vec!["next"], "http://api.x.io/orders/43"),
//!     ]);
//! ```

extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[cfg(feature="http-compat")]
extern crate http;

pub mod action;
pub mod entity;
pub mod field;
pub mod link;
pub mod method;
pub mod property;

pub mod prelude {
    pub use super::action::Action;
    pub use super::entity::Entity;
    pub use super::field::Field;
    pub use super::link::Link;
    pub use super::method::Method;
    pub use super::property::Property;
}
