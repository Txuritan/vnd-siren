# vnd-SIREN

vnd-SIREN is a SIREN DSL/serializer for Rust.

## Examples

Recreating the sample given on the SIREN repository:

```rust
let siren = Entity::builder()
    .class("order")
    .properties(vec![
        Property::new("itemCount", 3),
        Property::new("orderNumber", 42),
        Property::new("status", "pending"),
    ])
    .actions(vec![
        Action::builder("add-item", "http://api.x.io/orders/42/items")
            .method(Method::Post)
            .title("Add Item")
            .typ("application/x-www-form-urlencoded")
            .fields(vec![
                Field::builder("orderNumber").typ("hidden").value("42"),
                Field::builder("productCode").typ("text"),
                Field::builder("quantity").typ("number"),
            ]),
    ])
    .entities(vec![
        Entity::builder()
            .rel(vec!["http://x.io/rels/order-items"])
            .classes(vec!["items", "collection"])
            .href("http://api.x.io/orders/42/items"),
        Entity::builder()
            .rel(vec!["http://x.io/rels/customer"])
            .classes(vec!["info", "customer"])
            .links(vec![
                Link::builder(vec!["self"], "http://api.x.io/customers/pj123"),
            ])
            .properties(vec![
                Property::new("customerId", "pj123"),
                Property::new("name", "Peter Joseph"),
            ]),
    ])
    .links(vec![
        Link::builder(vec!["self"], "http://api.x.io/orders/42"),
        Link::builder(vec!["previous"], "http://api.x.io/orders/41"),
        Link::builder(vec!["next"], "http://api.x.io/orders/43"),
    ]);
```